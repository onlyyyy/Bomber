const net = require('net')
const tools = require('../tools/define');
const moment = require('moment');



class Socket {
    constructor() {
       
    }

    // fn (result) {
    //     return new Promise((resovle , reject)=>{
    //         setTimeout(() => {
    //             console.log("result = ",result);
    //         }, 5000);
    //     })
    // }

    init (ip,port) {
        this.server = new net.createServer();
        this.server.on('connection', async(client) => {

            console.log("收到一个连接 ");
            client.on('data',async function (msg) { //接收client发来的信息

                
                let resValue = JSON.parse(msg)
               // resValue = iconv.decode(msg,'utf8');
                console.log(`客户端发来一个信息：${resValue}`);
                console.log("resValue.type = ",resValue.type);
                let type = parseInt(resValue.type);       //与客户端预先定义接口
                let result = null;
                switch (type) {
                    case 1000:
                        //读写Json创建新的连接
                        result = await instance.tcpHandler.addNewConn(resValue.value,tools.fileName);
                        console.log("result = ",result);

                        client.write(JSON.stringify(result));
                        break;
                    case 1001:
                        //返给Qt目前选择的配置信息
                        result = await instance.tcpHandler.readConn(resValue.value,tools.fileName);
                        console.log("result = ",result);
                        client.write(JSON.stringify(result));
                        break;
                    case 1002:
                        //服务器压力测试
                        result = await instance.tcpHandler.bench(resValue.value,client)
                        // await tools.sleep(1000);
                        // console.log("result = ",result);
                        // client.write(JSON.stringify(result));
                        
                        break;

                    default:
                        break;
                }
                

                
            });

            client.on('error', function (e) { //监听客户端异常
                console.log('client error' + e);
                client.end();
            });

            client.on('close', function () {

                console.log(`客户端下线了`);
            });

        });
        this.server.listen( port,ip,function () {
            console.log(`Tcp服务器运行在：http://${ip}:${port}`);
          });

    }
    
}

module.exports = Socket;