const applyMysql = require('./tools/applyMysql');
const Socket = require('./tcp/socket');
const TcpManager = require('./tcp/TcpManager');

const Http = require('./http/http');
const HttpManager = require('./http/HttpManager');
async function init() {
    let mysqlInstance = new applyMysql();
    let socket = new Socket();
    let http = new Http();
    let tcpManager = new TcpManager();
    let httpManager = new HttpManager();
    global.instance = {
        dbHandler : mysqlInstance,
        socketHandler : socket,
        httpHandler : http,
        tcpHandler : tcpManager,
        httpHandler : httpManager
    };
    socket.init("127.0.0.1","13002");
    http.start();
}

async function main() {
    init();



}

main();