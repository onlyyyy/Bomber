const moment = require('moment')
const fs = require('fs')
const conv = require('iconv-lite');
const childProcess = require('child_process');
const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require('constants');

class TcpManager {
    constructor() {
        
    }

    async addNewConn(JsonBody,fileName) {
        try{
            console.log('fileName = ',fileName);
            if(fs.existsSync(fileName)==false)
            {
                fs.writeFileSync(fileName,"");
            }
            console.log("JsonBody = ",JsonBody);

            let fileInfo = fs.readFileSync(fileName).toString();
            console.log("读取出来的文件为",fileInfo);
            let FileJson;
            try {
                FileJson = JSON.parse(fileInfo);
            }catch (error) {
                console.log("不是合法的Json");
                FileJson = [];
            }

            let resValue = {};
            resValue.connNum = JsonBody.connNum;
            resValue.ip = JsonBody.ip;
            resValue.port = JsonBody.port;
            resValue.protType = JsonBody.protType;
            let message ;
            if(!FileJson.length)
            {
                let temp = {};
                temp[JsonBody.name] = resValue;
                FileJson.push(temp) //赋值
                message = "0"
            }
            for(let i=0;i<FileJson.length;i++)
            {
                let res = FileJson[i];
                if(res[JsonBody.name]!=undefined)   //已存在项目
                {
                    res[JsonBody.name] = resValue;
                    FileJson[i] = res;
                    message = "1";
                    break;
                }
                else if(i == FileJson.length -1)
                {
                    let temp = {};
                    temp[JsonBody.name] = resValue;
                    FileJson.push(temp) //赋值
                    message = "0";
                }
            }

            
            
            
            console.log("写入的Json为",FileJson);
            fs.writeFileSync(fileName,JSON.stringify(FileJson));
            let nowTime = moment().format("HH:mm:ss");
            return {
                "type":"1000",
                "result":message,
                "time":nowTime
            };
            
        }catch (error) {
            console.log("error->",error);
            return {
                "type":"1000",
                "result":"-1",
                "time":nowTime
            }
        }
        

    }

    async readConn(Name,fileName) {
        let nowTime =  moment().format("HH:mm:ss");
        try{
            let fileInfo = fs.readFileSync(fileName).toString();
            fileInfo = JSON.parse(fileInfo);

            for(let i=0;i<fileInfo.length;i++)
            {
                let f = fileInfo[i];
                
                for(let key in f)
                {
                    if(key == Name)
                    {
                        //之前定义的Json格式不理想，现在重定义
                        let temp = {};
                        temp.name = Name;
                        temp.value = {};
                        temp.value.connNum = f[key].connNum;
                        temp.value.ip = f[key].ip;
                        temp.value.port = f[key].port;
                        temp.value.protType = f[key].protType;

                        return {
                            "type":"1001",
                            "result":temp,
                            "time" : nowTime
                        };
                    }
                }
            }
        }catch (error) {
            console.log("error->",error);
            return {
                "type":"1001",
                "result":-1,
                "time" : nowTime
            };
        }
        
    }


    async bench(JsonBody,client) { 
        console.log("JsonBody = ",JsonBody);
        let ip = JsonBody.ip;
        let port = JsonBody.port;
        let num = JsonBody.connNum; 
        let type = JsonBody.protType;
        let child;
        let success = 0,error = 0,unknown = 0,receive = 0;
        let nowTime = moment().format("HH:mm:ss");
        let resValue = {};
        for(let i=0;i<num;i++)
        {
            if(type == 0)
            {
                child = childProcess.fork('./TcpBench.js');
            }
            else
            {
                child = childProcess.fork('./HttpBench.js');
            }

            child.on('message',async (msg)=>{
                
                if(msg.result == "success")
                {
                    
                    success ++;
                }
                else if(msg.result == "error")
                {
                    error++;
                }
                else if(msg.result == "receive")
                {
                    receive++;
                }
                if(i == num-1)
                {
                    unknown = num - success - error;

                    
                    
                    resValue.type = "1002";
                    resValue.time = nowTime;
                    resValue.result = {
                        "success":success,
                        "error":error,
                        "receive":receive,
                        "lost":unknown
                    }
                    console.log("resValue = ",resValue);
                    client.write(JSON.stringify(resValue));
                    return;
                }
            })
            
            child.send({
                "ip":ip,
                "port":port
            })
            
        }
        
        
        //protType 0:Tcp
    }
}

module.exports = TcpManager;

