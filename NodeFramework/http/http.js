const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const koaStatic = require('koa-static');
const indexRouter = require('./index');
let Router = require('koa-router');



const koaBody = require('koa-body');

class Http {
    constructor() {
        
    }

    start() {
        const app = new Koa();
        app.use(bodyParser());

        var router = new Router();
        router.use('/',indexRouter);
        app.use(router.routes());
        app.use(router.allowedMethods());

        let port = 13000;
        app.listen(port, () => {
            console.log("http服务器已启动，http://127.0.0.1:",port);
        })
    }
}


module.exports = Http;