//#ifndef BOMBER_H
//#define BOMBER_H
#pragma once
#include <QMainWindow>
#include "common.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Bomber; }
QT_END_NAMESPACE

class Bomber : public QMainWindow
{
    Q_OBJECT

public:
    Bomber(QWidget *parent = nullptr);
    ~Bomber();

    void initConnList();

    void setStatus(); //设置label的文字和颜色

    void getAddConnResult(QString resValue,QString time);    //获取添加新配置的结果
    void getClickConnResult(QString resValue,QString time);
    void getBenchResult(QString resValue,QString time);      //获取压力测试的结果
private slots:
    void on_btn_close_clicked();

    void on_btn_save_clicked();

    void on_btn_reconn_clicked();

    void whileConnected();

    void whileDisconnected();

    void displayError(QAbstractSocket::SocketError);

    void read();

    void on_btn_send_clicked();



    void on_listView_conn_clicked(const QModelIndex &index);

private:
    Ui::Bomber *ui;
    QTcpSocket * tcpSocket;
    bool connected;
    DJY::EasyQJson *easy;
    QStandardItemModel * connList;
#ifdef QT_NO_DEBUG
    QString fileName = "./conn.json";
#elif defined (Q_OS_MACOS)

    QString fileName = "/Users/hideyoshi/Desktop/codes/Bomber/conn.json";
#else
    QString fileName = "E:/gitee/Bomber/conn.json";
#endif
};
//#endif // BOMBER_H
