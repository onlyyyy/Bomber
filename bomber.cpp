#include "bomber.h"
#include "ui_bomber.h"
const QString connIp = "127.0.0.1";
const int connPort = 13002;
Bomber::Bomber(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Bomber)
{
    ui->setupUi(this);
    ui->textEdit_result->setPlaceholderText("结果一览");
    ui->spinBox_connNum->setValue(1000);
    ui->lineEdit_name->setPlaceholderText("配置名称");
    tcpSocket = new QTcpSocket;
    tcpSocket->abort();
    connected = false;
    setStatus();
    initConnList();
    tcpSocket->connectToHost(connIp,connPort,QTcpSocket::ReadWrite);
    connect(tcpSocket,SIGNAL(connected()),this,SLOT(whileConnected()));
    connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(whileDisconnected()));
    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
    connect(tcpSocket,&QTcpSocket::readyRead,this,&Bomber::read);
}

Bomber::~Bomber()
{
    delete ui;
}

void Bomber::initConnList()
{


    QJsonArray array = easy->readJsonFileReturnArray(fileName);



    QList<QStandardItem *>item;
    connList = new QStandardItemModel;
    for(int i=0;i<array.size();i++)
    {
        QJsonValue v = array.at(i);
        QJsonObject ob = v.toObject();
        QStringList list = ob.keys();

        QStandardItem * itemi = new QStandardItem(list[0]);

        item<<itemi;
        connList->insertRow(i,itemi);
    }

    ui->listView_conn->setModel(connList);
}


void Bomber::on_btn_close_clicked()
{
    close();
}

void Bomber::on_btn_save_clicked()
{

    QString name = ui->lineEdit_name->text();
    if(name == "nullptr" || name == "" )
    {
        QMessageBox::warning(nullptr,"警告","配置名称不能为空");
        return;
    }
    bool b = name.contains(QRegExp("[\\x4e00-\\x9fa5]+"));
    if(b)
    {
        //存在中文
        QMessageBox::warning(nullptr,"警告","因为微软的技术能力问题，配置名称不能有中文");
        return;
    }
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","服务器已离线");
        return;
    }
    QString ip = ui->lineEdit_ip->text();
    QString port = ui->lineEdit_port->text();
    int protType = ui->comboBox->currentIndex();
    int connNum = ui->spinBox_connNum->value();
    if(ip == "" || ip ==nullptr||port == "" || port == nullptr)
    {
        QMessageBox::warning(nullptr,"警告","IP和端口不能为空");
        return;
    }
    QJsonObject ob,oc;
    ob["type"] = 1000;

    oc["name"] = name;
    oc["ip"] = ip;
    oc["protType"] = protType;
    oc["port"] = port;
    oc["connNum"] = connNum;
    ob["value"] = oc;
    QString temp = easy->readObjectReturnQString(ob);
    QByteArray send = temp.toLocal8Bit();

    tcpSocket->write(send);
    ui->lineEdit_name->clear();
    ui->lineEdit_name->repaint();
}

void Bomber::on_btn_reconn_clicked()
{
    tcpSocket->abort();
    connected = false;
    tcpSocket->connectToHost(connIp,connPort,QTcpSocket::ReadWrite);
    ui->btn_send->setText("发送!");
    ui->btn_send->setEnabled(true);
    ui->btn_send->repaint();
//    if(connected == false)
//    {
//        QMessageBox::warning(nullptr,"警告","重连服务器失败!");
//        return;
//    }
}

void Bomber::whileConnected()
{
    connected = true;
    setStatus();
}

void Bomber::whileDisconnected()
{
    connected = false;
    setStatus();
}


void Bomber::setStatus()
{
    if(connected == true)
    {
        ui->label_status->setText("在线");
        ui->label_status->setStyleSheet("color:green");
    }
    else
    {
        ui->label_status->setText("离线");
        ui->label_status->setStyleSheet("color:red");
    }
    ui->label_status->repaint();
}

void Bomber::displayError(QAbstractSocket::SocketError)
{
    QString error = tcpSocket->errorString();
    if(tcpSocket->error() == QAbstractSocket::RemoteHostClosedError)
    {
        return;     //这个错误在服务器状态中体现
    }
    QMessageBox::warning(nullptr,"警告",error);
//    qDebug()<<tcpSocket->error();
//    QString hint;
//    if(tcpSocket->error() == QAbstractSocket::RemoteHostClosedError)
//    {
//        hint = "服务器已关闭服务";
//    }
//    else if(tcpSocket->error() == QAbstractSocket::RemoteHostClosedError)
//    {

//    }
}


void Bomber::read()
{

    QString result = tcpSocket->readAll();
    qDebug()<<"服务器返回的结果为"<<result.toLocal8Bit();
    QJsonObject ob = easy->ReadQStringReturnObject(result.toLocal8Bit());

    QString test = ob["type"].toString();
    qDebug()<<"test = "<<test;
    qDebug()<<"value"<<ob["result"].toString();
    int res = test.toInt();
    QJsonObject temp =ob["result"].toObject();
    switch (res)
    {
        case 1000:
            getAddConnResult(ob["result"].toString(),ob["time"].toString());
        break;
        case 1001:

            getClickConnResult(easy->readObjectReturnQString(temp),ob["time"].toString());

        break;
        case 1002:

            getBenchResult(easy->readObjectReturnQString(temp),ob["time"].toString());
        break;
    default:
        qDebug()<<"switch in default";
        break;
    }

    ui->textEdit_result->repaint();
}

void Bomber::getAddConnResult(QString resValue,QString time)
{
    int value = resValue.toInt();
    QString message;
    if(value == 0)
    {
        message =time +" 创建配置成功";
    }
    else if(value == 1)
    {
        message =time +" 覆盖配置成功";
    }
    else
    {
        message =time +" 创建配置失败";

    }

    ui->textEdit_result->append(message);
    ui->textEdit_result->repaint();
    initConnList();
}

void Bomber::getClickConnResult(QString resValue,QString time)
{
    if(resValue == "-1")
    {
        ui->textEdit_result->append(time+" 读取配置失败");
    }
    else
    {
        QJsonObject root = easy->ReadQStringReturnObject(resValue);
        qDebug()<<"resValue = "<<resValue;
        QJsonObject value = root["value"].toObject();
        int connNUm = value["connNum"].toInt();
        QString  ip = value["ip"].toString();
        QString  port = value["port"].toString();
        int protType = value["protType"].toInt();
        ui->lineEdit_ip->setText(ip);
        ui->lineEdit_ip->repaint();
        ui->lineEdit_port->setText(port);
        ui->lineEdit_port->repaint();
        ui->comboBox->setCurrentIndex(protType);
        ui->spinBox_connNum->setValue(connNUm);
        ui->comboBox->repaint();
        ui->spinBox_connNum->repaint();
        ui->textEdit_result->append(time+" 读取配置成功");
    }


}

void Bomber::getBenchResult(QString resValue,QString time)
{

    QJsonObject ob = easy->ReadQStringReturnObject(resValue);

    int success = ob["success"].toInt();
    int error = ob["error"].toInt();
    int lost = ob["lost"].toInt();
    int receive = ob["receive"].toInt();
    ui->textEdit_result->append(time+ " 压力测试的结果: "+QString::number(success)+"个连接成功 "+QString::number(error)+"个连接失败 "+
                                QString::number(lost)+"个连接失败 "+QString::number(receive)+"个连接收到应答");
    ui->textEdit_result->repaint();
    ui->btn_send->setText("发送!");
    ui->btn_send->setEnabled(true);
    ui->btn_send->repaint();
}

void Bomber::on_btn_send_clicked()
{
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","服务器已离线");
        return;
    }
    QString ip = ui->lineEdit_ip->text();
    QString port = ui->lineEdit_port->text();
    int protType = ui->comboBox->currentIndex();
    int connNum = ui->spinBox_connNum->value();
    if(ip == "" || ip ==nullptr||port == "" || port == nullptr)
    {
        QMessageBox::warning(nullptr,"警告","IP和端口不能为空");
        return;
    }
    QJsonObject ob,oc;
    ob["type"] = 1002;

    oc["ip"] = ip;
    oc["protType"] = protType;
    oc["port"] = port;
    oc["connNum"] = connNum;
    ob["value"] = oc;
    QByteArray send = easy->readObjectReturnQString(ob).toLocal8Bit();
    tcpSocket->write(send);
    ui->btn_send->setText("等待结果..");
    ui->btn_send->setEnabled(false);
    ui->btn_send->repaint();
}

void Bomber::on_listView_conn_clicked(const QModelIndex &index)
{
    QString text = index.data().toString();
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","连接服务器失败!");
        return;
    }

    ui->lineEdit_name->setText(text);
    ui->lineEdit_name->repaint();
    QJsonObject ob;
    ob["type"] = 1001;
    ob["value"] = text;
    QString send = easy->readObjectReturnQString(ob);
    tcpSocket->write(send.toLocal8Bit());
}
