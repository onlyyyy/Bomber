const net = require('net');
const { exit } = require('process');

process.on('message',(m)=>{
    let ip = m.ip;
    let port = m.port;
    try{
        let client = new net.Socket();
        client.setEncoding('utf8');
        client.connect(port,ip,(error)=>{
            client.write("hello");
            process.send({
                result:"success"
            })
            if(error)
            {
                process.send({
                    result:"error"
                })
            }

        })
        client.on('data',(data)=>{
            process.send({
                result:"receive"
            })
            client.destroy();

        })
        client.on('error',(data)=>{
            process.send({
                result:"error"
            })
            client.destroy();
        })

    }catch (error) {
        console.log("error->",error);
        process.send({
            result:"error"
        })
        status = true;
    }
    
})
